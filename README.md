**Indicaciones de instalacion de paquetes necesarios respecto al Sistema operativo:**

*Se utilizo la version de python mas estable recientemente Python3.7.3.
Estamos utilizando una bdd remota en azure conectandonos mediante mysql.*

**Windows:**
1. Necesitamos instalar python3 en nuestra maquina, para lo cual podemos ir a la pagina oficial y descargarlo (https://www.python.org/downloads/), o descargarlo directamente desde este enlace:
    https://www.python.org/ftp/python/3.7.3/python-3.7.3.exe
    * a la hora de instalar Python podemos seleccionar la opcion de instalar tkinter, debemos asegurarnos que este seleccionada.
2. Antes de continuar debemos revisar si al instalar python creamos una variable de entorno para el, si no es asi, debemos hacerlo manualmente.
3. Ahora instalaremos mysqlconnector para poder conectarnos a la base de datos, para lo cual podemos acceder a descargarlo de la pagina oficial (https://dev.mysql.com/downloads/file/?id=485117), o seguir el siguiente enlace:
    https://dev.mysql.com/get/Downloads/Connector-Python/mysql-connector-python-8.0.16-py3.7-windows-x86-32bit.msi
    

**Linux:**
1. Instalamos python3 mediante el siguiente comando:
    sudo apt install python3
2. Tambien necesitaremos tkinter para la creacion de interfaces:
    sudo apt-get install python3-tk
3. Procedemos a instalar pip como manejador de paquetes para instalar y administrar paquetes de software escritos en Python:
    sudo apt install python3-pip
4. Por ultimo instalamos el paquete requido para usar mysql:
    sudo pip3 install mysql-connector-python-rf
    